%include "sseutils.nasm"

section .data
k equ 8
v equ 12
ris equ 16

section .bss

section .text

global prodottovtv

prodottovtv: 

	push	ebp							; salva il Base Pointer
    mov		ebp, esp					; il Base Pointer punta al Record di Attivazione corrente
    push	ebx							; salva i registri da preservare
    push	esi
    push	edi
            
    mov edi, [ebp+k+8]
	mov eax, [ebp+v+8]
	mov ebx, [ebp+ris+8]
	mov edx, edi
	sub edx, 16
	xor esi, esi
	xorps xmm0, xmm0
	
fori:   

    cmp esi, edx
    jg ciclo_i
    movaps xmm0,[eax+esi*4]
    mulps  xmm0,xmm0
    addps  xmm1,xmm0
    inc    esi
    cmp    esi,edi
    jl     fori
    movss  [ebx],xmm1
    
ciclo_i:

	cmp esi, edi	
	jg fine
	movss xmm1, [ebx+esi*4]
	mulss xmm1,xmm1
	addss xmm0,xmm1
	inc esi
	jmp ciclo_i

fine:

	; ------------------------------------------------------------
	; Sequenza di uscita dalla funzione
	; ------------------------------------------------------------

	pop	edi									; ripristina i registri da preservare
	pop	esi
    pop	ebx
	mov	esp, ebp							; ripristina lo Stack Pointer
	pop	ebp									; ripristina il Base Pointer
	ret										; torna alla funzione C chiamante

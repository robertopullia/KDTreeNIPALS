%include "sseutils64.nasm"

section .data
k equ 8
v equ 12
ris equ 16

section .bss

section .text

global prodottovtv

prodottovtv: 

	push	rbp							; salva il Base Pointer
    mov		rbp, rsp					; il Base Pointer punta al Record di Attivazione corrente
    push	rbx							; salva i registri da preservare
    push	rsi
    push	rdi
            
    mov rdi, [rbp+k+8]
	mov rax, [rbp+v+8]
	mov rbx, [rbp+ris+8]
	mov rdx, rdi
	sub rdx, 16
	xor rsi, rsi
	vxorps xmm0, xmm0
	
fori:   

    cmp rsi, rdx
    jg ciclo_i
    vmovaps xmm0,[rax+rsi*4]
    vmulps  xmm0,xmm0
    vaddps  xmm1,xmm0
    inc    rsi
    cmp    rsi,rdi
    jl     fori
    vmovss  [rbx],xmm1
    
ciclo_i:

	cmp rsi, rdi	
	jg fine
	vmovss xmm1, [rbx+rsi*4]
	vmulss xmm1,xmm1
	vaddss xmm0,xmm1
	inc rsi
	jmp ciclo_i

fine:

	; ------------------------------------------------------------
	; Sequenza di uscita dalla funzione
	; ------------------------------------------------------------

	pop	rdi									; ripristina i registri da preservare
	pop	rsi
    pop	rbx
	mov	rsp, rbp							; ripristina lo Stack Pointer
	pop	rbp									; ripristina il Base Pointer
	ret										; torna alla funzione C chiamante
